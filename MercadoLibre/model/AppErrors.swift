//
//  AppErrors.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.
//

import Foundation

enum ServiceError: String {
    case noConnectionError = "Você parece não estar conectado a internet. Por favor verifique e tente novamente."
    case genericError = "Erro de serviço. Por favor tente novamente."
    case parseError = "Error ao tratar retorno. Por favor verifique"
}

struct UIError: Error {
    static let noTextSearchBarError = "Por favor digite um produto no campo de busca."
}

struct UIMessages: Error {
    static let errorTitle = "Erro"
    static let noResultsFound = "Sua busca não retornou itens. Por favor tente novamente."
}
