//
//  SearchProductsListPresenter.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.
//

import Foundation

class SearchProductsListPresenter {
    var view: SearchProductsListViewController!
    let networkServices = NetworkServices()
    
    func addView(view: SearchProductsListViewController) {
        self.view = view
    }
    
    func searchProduct(searchText: String) {
        view.showLoading()
        
        let url = APIConstants.urlSearchProduct + searchText.replacingSpacesToURLEncoded()
        
        self.networkServices.doGETRequest(url: url, onSuccess: { (response) in
            //PARSE
            
            if(response.count > 0) {
                self.view.lblHelpMsg.isHidden = true
                self.view.arrProducts = response
                self.view.tableView.reloadData()
            }else {
                self.view.lblHelpMsg.isHidden = false
            }
            
            self.view.hideLoading()
        }) { (error) in
            self.view.hideLoading()
            self.view.showAlert(title: UIMessages.errorTitle, description: error.rawValue)
        }
    }
}
