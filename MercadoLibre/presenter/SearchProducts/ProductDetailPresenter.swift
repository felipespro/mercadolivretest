//
//  ProductDetailPresenter.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.

import Foundation
import UIKit

class ProductDetailPresenter {
    var view: ProductDetailViewController!
    
    func addView(view: ProductDetailViewController) {
        self.view = view
    }
    
    func populateProduct(product: MLProduct) {
        let imgURL = URL(string: product.thumbNail)
        self.view.imgProduct.kf.setImage(with: imgURL)
        
        self.view.lblProductTitle.text = product.title
        self.view.lblFullPrice.text = "Valor: \(product.price.toStringDollars())"
        let installMentsPrice = Double(round(100*(product.price / Double(product.installments.quantity)))/100)
        self.view.lblInstallments.text = "\(product.installments.quantity)x \(installMentsPrice.toStringDollars())"
        self.view.lblSellerReputation.text = "Vendedor: \(product.sellerReputation.capitalized)"
    }
    
    func goToProductPage(urlString: String) {
        self.view?.showAlert(title: "Atenção", description: "Você será direcionado para uma página web. Deseja continuar?", actionYes: {_ in
            if let url = URL(string: urlString) {
                UIApplication.shared.open(url, options: [:])
            }else {
                print("Erro ao abrir página externa.")
            }
        }, actionNo: {_ in
            
        })
    }
}
