//
//  Extensions.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.

import Foundation

extension String {
    func replacingSpacesToURLEncoded() -> String {
        return self.replacingOccurrences(of: " ", with: "%20")
    }
}
