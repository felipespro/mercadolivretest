//
//  UIExtensions.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.
//

import UIKit

extension UIViewController {
    
    func configDefaultNavBar() {
        let nav = self.navigationController?.navigationBar
//        self.navigationItem.setHidesBackButton(true, animated:false);
        
        nav?.barStyle = UIBarStyle.default
        nav?.tintColor = UIColor.black
        nav?.barTintColor = UIColor.MLColors.Yellow
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 120.0, height: 40.0))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 120.0, height: 40.0))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "imgNavBarLogo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    
    @objc func performSegueToReturnBack()  {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showAlert(title: String, description: String) {
        let alertController = UIAlertController(
            title: title,
            message: description,
            preferredStyle: .alert
        )
        
        let dismissAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(dismissAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(title: String, description: String, actionYes: @escaping (UIAlertAction) -> Void, actionNo: @escaping (UIAlertAction) -> Void) {
        let alertController = UIAlertController(
            title: title,
            message: description,
            preferredStyle: .alert
        )
        
        let actionNo = UIAlertAction(title: "Cancelar", style: .default, handler: actionNo)
        let actionYes = UIAlertAction(title: "Continuar", style: .default, handler: actionYes)
        alertController.addAction(actionNo)
        alertController.addAction(actionYes)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension UIColor {
    struct MLColors {
        static let Yellow = UIColor(red: 255, green: 239, blue: 116, alpha: 1)
    }
}
