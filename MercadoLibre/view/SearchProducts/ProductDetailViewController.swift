//
//  ProductDetailViewController.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.

import UIKit

//MARK: -PRODUCT DETAIL INTERFACES

protocol ProductDetailDelegate {
    func populateProduct()
    func goToProductPage()
}

//MARK: -VIEW CONTROLLER

class ProductDetailViewController: UIViewController, UINavigationBarDelegate {
    
//MARK: -PROPERTIES
    static let identifier = "productDetailVC"
    var product: MLProduct!
    let presenter = ProductDetailPresenter()
    
//MARK: -IBOUTLETS
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductTitle: UILabel!
    @IBOutlet weak var lblFullPrice: UILabel!
    @IBOutlet weak var lblInstallments: UILabel!
    @IBOutlet weak var lblSellerReputation: UILabel!
    
//MARK: -LIFECYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.addView(view: self)
        self.configDefaultNavBar()
        self.populateProduct()
    }
    
//MARK: -ACTION METHODS
    
    @IBAction func btnProductDetailTouched(_ sender: Any) {
        self.goToProductPage()
    }
}

//MARK: -INTERFACE IMPLEMENTATION

extension ProductDetailViewController: ProductDetailDelegate {
    func populateProduct() {
        presenter.populateProduct(product: self.product)
    }
    
    func goToProductPage() {
        presenter.goToProductPage(urlString: self.product.permaLink)
    }
}
